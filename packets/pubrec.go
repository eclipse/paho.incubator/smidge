/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type PubrecMessage struct {
	Header
	MessageId uint16
}

func (p *PubrecMessage) MessageType() byte {
	return PUBREC
}

func (p *PubrecMessage) Write(w io.Writer) error {
	packet := p.Header.pack()
	packet.WriteByte(PUBREC)
	packet.Write(encodeUint16(p.MessageId))
	_, err := packet.WriteTo(w)

	return err
}

func (p *PubrecMessage) Unpack(b io.Reader) {
	p.MessageId = readUint16(b)
}
