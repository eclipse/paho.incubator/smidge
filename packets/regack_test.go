/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestRegackStruct(t *testing.T) {
	msg := NewMessage(REGACK).(*RegackMessage)

	if assert.NotNil(t, msg, "New message should not be nil") {
		assert.Equal(t, "*packets.RegackMessage", reflect.TypeOf(msg).String(), "Type should be RegackMessage")
		assert.Equal(t, 7, msg.Length, "Default Length should be 7")
		assert.Equal(t, 0, msg.TopicId, "Default TopicId should be 0")
		assert.Equal(t, 0, msg.MessageId, "Default MessageId should be 0")
		assert.Equal(t, 0, msg.ReturnCode, "Default ReturnCode should be 0")

		assert.Equal(t, REGACK, msg.MessageType(), "MessageType() should return REGACK")
	}
}
