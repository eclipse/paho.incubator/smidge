/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type AdvertiseMessage struct {
	Header
	GatewayId byte
	Duration  uint16
}

func (a *AdvertiseMessage) MessageType() byte {
	return ADVERTISE
}

func (a *AdvertiseMessage) Write(w io.Writer) error {
	packet := a.Header.pack()
	packet.WriteByte(ADVERTISE)
	packet.WriteByte(a.GatewayId)
	packet.Write(encodeUint16(a.Duration))
	_, err := packet.WriteTo(w)

	return err
}

func (a *AdvertiseMessage) Unpack(b io.Reader) {
	a.GatewayId = readByte(b)
	a.Duration = readUint16(b)
}
