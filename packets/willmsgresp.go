/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type WillMsgRespMessage struct {
	Header
	ReturnCode byte
}

func (wm *WillMsgRespMessage) MessageType() byte {
	return WILLMSGRESP
}

func (wm *WillMsgRespMessage) Write(w io.Writer) error {
	packet := wm.Header.pack()
	packet.WriteByte(WILLMSGRESP)
	packet.WriteByte(wm.ReturnCode)
	_, err := packet.WriteTo(w)

	return err
}

func (wm *WillMsgRespMessage) Unpack(b io.Reader) {
	wm.ReturnCode = readByte(b)
}
