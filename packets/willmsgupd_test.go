/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestWillMsgUpdateStruct(t *testing.T) {
	msg := NewMessage(WILLMSGUPD).(*WillMsgUpdateMessage)

	if assert.NotNil(t, msg, "New message should not be nil") {
		assert.Equal(t, "*packets.WillMsgUpdateMessage", reflect.TypeOf(msg).String(), "Type should be WillMsgUpdateMessage")
		assert.Equal(t, []byte(nil), msg.WillMsg, "Default WillMsg should be blank")

		assert.Equal(t, WILLMSGUPD, msg.MessageType(), "MessageType() should return WILLMSGUPD")
	}
}
