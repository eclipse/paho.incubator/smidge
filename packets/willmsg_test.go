/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestWillMsgStruct(t *testing.T) {
	msg := NewMessage(WILLMSG).(*WillMsgMessage)

	if assert.NotNil(t, msg, "New message should not be nil") {
		assert.Equal(t, "*packets.WillMsgMessage", reflect.TypeOf(msg).String(), "Type should be WillMsgMessage")
		assert.Equal(t, []byte(nil), msg.WillMsg, "Default WillMsg should be blank")

		assert.Equal(t, WILLMSG, msg.MessageType(), "MessageType() should return WILLMSG")
	}
}
