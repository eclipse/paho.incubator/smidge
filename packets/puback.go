/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type PubackMessage struct {
	Header
	TopicId    uint16
	MessageId  uint16
	ReturnCode byte
}

func (p *PubackMessage) MessageType() byte {
	return PUBACK
}

func (p *PubackMessage) Write(w io.Writer) error {
	packet := p.Header.pack()
	packet.WriteByte(PUBACK)
	packet.Write(encodeUint16(p.TopicId))
	packet.Write(encodeUint16(p.MessageId))
	packet.WriteByte(p.ReturnCode)
	_, err := packet.WriteTo(w)

	return err
}

func (p *PubackMessage) Unpack(b io.Reader) {
	p.TopicId = readUint16(b)
	p.MessageId = readUint16(b)
	p.ReturnCode = readByte(b)
}
