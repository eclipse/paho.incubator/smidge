/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestSearchGwStruct(t *testing.T) {
	msg := NewMessage(SEARCHGW).(*SearchGwMessage)

	if assert.NotNil(t, msg, "New message should not be nil") {
		assert.Equal(t, "*packets.SearchGwMessage", reflect.TypeOf(msg).String(), "Type should be SearchGwMessage")
		assert.Equal(t, 3, msg.Length, "Default Length should be 3")
		assert.Equal(t, 0, msg.Radius, "Default Radius should be 0")

		assert.Equal(t, SEARCHGW, msg.MessageType(), "MessageType() should return SEARCHGW")
	}
}
