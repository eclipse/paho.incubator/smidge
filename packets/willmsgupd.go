/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type WillMsgUpdateMessage struct {
	Header
	WillMsg []byte
}

func (wm *WillMsgUpdateMessage) MessageType() byte {
	return WILLMSGUPD
}

func (wm *WillMsgUpdateMessage) Write(w io.Writer) error {
	wm.Header.Length = uint16(len(wm.WillMsg) + 2)
	packet := wm.Header.pack()
	packet.WriteByte(WILLMSGUPD)
	packet.Write(wm.WillMsg)
	_, err := packet.WriteTo(w)

	return err
}

func (wm *WillMsgUpdateMessage) Unpack(b io.Reader) {
	b.Read(wm.WillMsg)
}
