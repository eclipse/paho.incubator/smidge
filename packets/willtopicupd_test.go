/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestWillTopicUpdateStruct(t *testing.T) {
	msg := NewMessage(WILLTOPICUPD).(*WillTopicUpdateMessage)

	if assert.NotNil(t, msg, "New message should not be nil") {
		assert.Equal(t, "*packets.WillTopicUpdateMessage", reflect.TypeOf(msg).String(), "Type should be WillTopicUpdateMessage")
		assert.Equal(t, 0, msg.Qos, "Default Qos should be 0")
		assert.Equal(t, false, msg.Retain, "Default Retain flag should be false")
		assert.Equal(t, []byte(nil), msg.WillTopic, "Default WillMsg should be blank")

		assert.Equal(t, WILLTOPICUPD, msg.MessageType(), "MessageType() should return WILLTOPICUPD")
	}

}
