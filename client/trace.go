/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package smidge

import (
	"io/ioutil"
	"log"
)

var (
	ERROR    *log.Logger
	CRITICAL *log.Logger
	WARN     *log.Logger
	DEBUG    *log.Logger
)

const (
	NET = "[net]     "
	PNG = "[pinger]  "
	CLI = "[client]  "
	DEC = "[decode]  "
	MES = "[message] "
	STR = "[store]   "
	MID = "[msgids]  "
	TST = "[test]    "
	STA = "[state]   "
	ERR = "[error]   "
)

func init() {
	ERROR = log.New(ioutil.Discard, "", 0)
	CRITICAL = log.New(ioutil.Discard, "", 0)
	WARN = log.New(ioutil.Discard, "", 0)
	DEBUG = log.New(ioutil.Discard, "", 0)
}
