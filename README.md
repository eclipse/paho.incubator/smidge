Smidge
======

An [MQTT-SN](http://mqtt.org/new/wp-content/uploads/2009/06/MQTT-SN_spec_v1.2.pdf) implementation in Go

Including
---------
- MQTT-SN Client Library (in progress)
- MQTT-SN Aggregating and Transparent Gateway

Download
--------
Clone this repo or `go get` with

```
go get git.eclipse.org/gitroot/paho.incubator/smidge.git/client
```

The same URL is used to import the library into your Go programs

```go
import (
	"git.eclipse.org/gitroot/paho.incubator/smidge.git/client"
	"git.eclipse.org/gitroot/paho.incubator/smidge.git/packets"
)
```